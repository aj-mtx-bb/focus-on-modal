import { LightningElement } from 'lwc';

export default class ModalFocusDemo extends LightningElement {
    showModal;
    lastFocussedElement;

    handleShowModal(event) {
        // Store the reference to the current element which is calling this modal
        // As modal will trap focus inside it, we can use this variable to focus the
        // last focussed element again when the modal is closed
        this.lastFocussedElement = event.target;
        this.showModal = true;
    }

    closeModal() {
        this.showModal = false;

        // Move focus back to the element which triggered modal
        this.lastFocussedElement?.focus();
    }
}