const FOCUSABLE_ELEMENTS = 'lightning-combobox, lightning-button-icon, button, lightning-helptext, lightning-input, lightning-button, lightning-textarea';

export function focusFirstElement(context, focusableElements = FOCUSABLE_ELEMENTS) {
    if( !context ) return;
    
    const modal = context.template.querySelector('.slds-modal');

    const firstFocusableElement = modal.querySelectorAll(focusableElements)[0];

    const focusableContent = modal.querySelectorAll(focusableElements);

    const lastFocusableElement = focusableContent[focusableContent.length - 1];

    firstFocusableElement.focus();

    context.template.addEventListener('keydown', function(event) {

        let isTabPressed = event.key === 'Tab' || event.keyCode === 9;
        if (!isTabPressed) {
            return;
        }

        if (event.shiftKey) { // if shift key pressed for shift + tab combination
            
            if (this.activeElement === firstFocusableElement) {
                lastFocusableElement.focus(); // add focus for the last focusable element
                event.stopPropagation()
                event.preventDefault();
            }
        } else { // if tab key is pressed
            if (this.activeElement === lastFocusableElement) { // if focused has reached to last focusable element then focus first focusable element after pressing tab
                firstFocusableElement.focus(); // add focus for the first focusable element
                event.preventDefault();
                event.stopPropagation()
            }
        }
    });
}