import { LightningElement, api } from 'lwc';
import { focusFirstElement } from 'c/util';

export default class SldsModal extends LightningElement {

    focusCalled;

    renderedCallback() {
        if( !this.focusCalled ) {
            focusFirstElement( this );
        }
    }

    handleClick() {
        this.dispatchEvent(new CustomEvent('close'));
    }
}